package com.spring.build;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.yaml.snakeyaml.Yaml;

import com.alibaba.druid.pool.DruidDataSourceFactory;
import com.alibaba.fastjson.JSON;
import com.spring.build.entity.Area;
import com.spring.build.service.IAreaService;
import com.spring.build.utils.IterationUtil;
import com.spring.build.utils.exceptions.ColLabeledEx;

import tk.mybatis.spring.mapper.MapperScannerConfigurer;


/**
 * 
 * jar包程序的入口
 *
 */
@Configuration
@ComponentScan(basePackages = "com.spring.build.service.impl")
public class App {
	
	private static final String[] MAPPER_LOCATIONS = {"tk.mybatis.mapper.common.base", "com.spring.build.mapper"};
	private static final String CONFIGPATH = "config" + File.separator + "qyFramework.yml";
	
	private static ApplicationContext context;
	
	private static void initApp() {
		context = new AnnotationConfigApplicationContext(App.class);
	}
	
	private static ApplicationContext getSpringContext() {
		if (context == null) {
			initApp();
		}
		return context;
	}
	
	public static <T> T getService(Class<T> clazz) {
		return getSpringContext().getBean(clazz);
	}
	
	public static void main( String[] args ) {
		/*IUserService userService = App.getService(IUserService.class);
		User user = userService.selectByPrimaryKey("System");
    	System.out.println(user);*/
    	
    	IAreaService areaService = App.getService(IAreaService.class);
    	List<Area> sources = areaService.selectAll();
    	try {
    		List<Area> list = IterationUtil.loop(sources);
    		String json = JSON.toJSONString(list);
    		System.out.println(json);
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (ColLabeledEx e) {
			e.printStackTrace();
		}
    	
    }
	
	@Bean("druidDataSource")//阿里巴巴数据池
	public DataSource druidDataSource() throws Throwable {
		Map<String, Object> configs = loadYaml();
		@SuppressWarnings("unchecked")
		Map<String, Object> druidConfig = (Map<String, Object>) configs.get("druid");
		Map<String, String> properties  = valueToStr(druidConfig);
		DataSource dataSource = DruidDataSourceFactory.createDataSource(properties);
		return dataSource;
	}
	
	@Bean("mapperScanner")//tx.mybatis扫描及配置
	public MapperScannerConfigurer mapperScanner(SqlSessionFactoryBean sqlSessionFactory) {
		MapperScannerConfigurer mapperScanner = new MapperScannerConfigurer();
		for (String mapperLocation : MAPPER_LOCATIONS) {
			mapperScanner.setBasePackage(mapperLocation);
		}
		mapperScanner.setSqlSessionFactoryBeanName("sqlSessionFactory");
		Properties properties = new Properties();
		properties.put("style", "normal");//驼峰不转下划线
		mapperScanner.setProperties(properties);
		return mapperScanner;
	}
	
	@Bean("transactionManager")  //将mybatis bean托管给Spring
    public DataSourceTransactionManager transactionManager(DataSource dataSource) {  
        return new DataSourceTransactionManager(dataSource);  
    }  
	
	@Bean("sqlSessionFactoryBean") //Spring扫描mybatis的bean
	public SqlSessionFactoryBean sqlSessionFactoryBean(DataSource dataSource) {
		SqlSessionFactoryBean sqlSessionFactory = new SqlSessionFactoryBean();
		sqlSessionFactory.setDataSource(dataSource);
		org.apache.ibatis.session.Configuration configuration = new org.apache.ibatis.session.Configuration();
		for (String mapperLocation : MAPPER_LOCATIONS) {
			configuration.addMappers(mapperLocation);
		}
		sqlSessionFactory.setConfiguration(configuration);
		return sqlSessionFactory;
	}
	
	@Bean("sqlSessionFactory")
	public SqlSessionFactory sqlSessionFactory(SqlSessionFactoryBean sqlSessionFactoryBean) throws Exception {
		return sqlSessionFactoryBean.getObject();
	}
	
    //Map<String, Object> 转换成  Map<String, String>
    private static Map<String, String> valueToStr(Map<String, Object> map) {
		Map<String, String> stringMap = new HashMap<String, String>();
		for (Entry<String, Object> entry : map.entrySet()) {
			String value = entry.getValue().toString();
			stringMap.put(entry.getKey(), value);
		}
		return stringMap;
	}
	
	@SuppressWarnings("unchecked") //读取yml配置文件
	private static Map<String, Object> loadYaml() {
		ClassLoader loader = App.class.getClassLoader();
		URL rootURL = loader.getResource("");
		String rootPath = rootURL.getPath();
		File config = new File(rootPath + File.separator + CONFIGPATH);
		FileInputStream input = null;
		Map<String, Object> map = null;
		try {
			input = new FileInputStream(config);
			Yaml yaml = new Yaml();
			map = (Map<String, Object>)yaml.load(input);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return map;
	} 
}
