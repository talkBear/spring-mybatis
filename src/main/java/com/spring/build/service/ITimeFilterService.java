package com.spring.build.service;

import com.spring.build.entity.TimeFilter;

public interface ITimeFilterService extends IBaseService<TimeFilter> {

}
