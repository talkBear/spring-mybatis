package com.spring.build.service;

import com.spring.build.entity.User;

public interface IUserService extends IBaseService<User> {

}
