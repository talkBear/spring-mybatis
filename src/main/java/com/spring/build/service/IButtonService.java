package com.spring.build.service;

import com.spring.build.entity.Button;

public interface IButtonService extends IBaseService<Button> {
	
}
