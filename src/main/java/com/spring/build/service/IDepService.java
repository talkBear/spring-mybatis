package com.spring.build.service;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.spring.build.entity.Department;

public interface IDepService extends IBaseService<Department> {
	
	/**
	 * 根据某个字段的关键字查询
	 * @param type 关键字类型 
	 * 		<p>0：部门名称	1：部门编号	2：部门简称	3：负责人	   4：电话号	5：分机号</p>
	 * 		<p><b>type为空或超出取值范围时，取0</b></p>
	 * @param key 关键字
	 * @return 组织list
	 */
	List<Department> selectByKey(Integer type, @NotNull String key);
	
}
