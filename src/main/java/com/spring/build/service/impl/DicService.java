package com.spring.build.service.impl;

import org.springframework.stereotype.Service;

import com.spring.build.entity.ProDictionary;
import com.spring.build.service.IDicService;

@Service
public class DicService  extends BaseService<ProDictionary> implements IDicService {

}
