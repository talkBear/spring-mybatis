package com.spring.build.service.impl;

import org.springframework.stereotype.Service;

import com.spring.build.entity.Relation;
import com.spring.build.service.IRelationService;

@Service
public class RelationService extends BaseService<Relation> implements IRelationService {

}
