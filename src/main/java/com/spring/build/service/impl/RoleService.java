package com.spring.build.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.build.entity.Role;
import com.spring.build.mapper.RoleMapper;
import com.spring.build.service.IRoleService;

@Service
public class RoleService extends BaseService<Role> implements IRoleService {

	@Autowired
	private RoleMapper mapper;
	
	public List<Role> selectByCate(Integer category) {
		return mapper.selectByCate(category);
	}

	public List<Role> selectByCateAndKey(Integer category, Integer type, String key) {
		return mapper.selectByCateAndKey(category, type, key);
	}

}
