package com.spring.build.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.build.entity.Area;
import com.spring.build.mapper.AreaMapper;
import com.spring.build.service.IAreaService;

@Service
public class AreaService extends BaseService<Area> implements IAreaService {
	
	@Autowired
	private AreaMapper mapper;

	public List<Area> selectMembers(String parentId) {
		return mapper.selectMembers(parentId);
	}
	
}
