package com.spring.build.service.impl;

import org.springframework.stereotype.Service;

import com.spring.build.entity.View;
import com.spring.build.service.IViewService;

@Service
public class ViewService extends BaseService<View> implements IViewService {

}
