package com.spring.build.service.impl;

import org.springframework.stereotype.Service;

import com.spring.build.entity.TimeFilter;
import com.spring.build.service.ITimeFilterService;

@Service
public class TimeFilterService extends BaseService<TimeFilter> implements ITimeFilterService {

}
