package com.spring.build.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.build.entity.RoleZone;
import com.spring.build.mapper.RoleZoneMapper;
import com.spring.build.service.IRoleZoneService;

@Service
public class RoleZoneService extends BaseService<RoleZone> implements IRoleZoneService {
	
	@Autowired
	private RoleZoneMapper mapper;
	
	public RoleZone selectByCateAndObject(Integer category, String objectId) {
		return mapper.selectByCateAndObject(category, objectId);
	}

}
