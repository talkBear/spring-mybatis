package com.spring.build.service.impl;

import org.springframework.stereotype.Service;

import com.spring.build.entity.Model;
import com.spring.build.service.IModelService;

@Service
public class ModelService extends BaseService<Model> implements IModelService {

}
