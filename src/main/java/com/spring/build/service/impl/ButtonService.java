package com.spring.build.service.impl;

import org.springframework.stereotype.Service;

import com.spring.build.entity.Button;
import com.spring.build.service.IButtonService;

@Service
public class ButtonService extends BaseService<Button> implements IButtonService {

}
