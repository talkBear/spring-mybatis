package com.spring.build.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.build.entity.Department;
import com.spring.build.mapper.DepMapper;
import com.spring.build.service.IDepService;

@Service
public class DepService extends BaseService<Department> implements IDepService {
	
	@Autowired
	private DepMapper mapper;
	
	public List<Department> selectByKey(Integer type, String key) {
		return mapper.selectByKey(type, key);
	}

}
