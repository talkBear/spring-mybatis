package com.spring.build.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.build.entity.AuthResource;
import com.spring.build.mapper.AuthResMapper;
import com.spring.build.service.IAuthResService;

@Service
public class AuthResService extends BaseService<AuthResource> implements IAuthResService {
	
	@Autowired
	private AuthResMapper mapper;

	public List<AuthResource> selectByCateAndObject(Integer category, String objectId) {
		return mapper.selectByCateAndObject(category, objectId);
	}

}
