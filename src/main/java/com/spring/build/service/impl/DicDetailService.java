package com.spring.build.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.build.entity.DicDetail;
import com.spring.build.mapper.DicDetailMapper;
import com.spring.build.service.IDicDetailService;

@Service
public class DicDetailService extends BaseService<DicDetail> implements IDicDetailService {

	@Autowired
	private DicDetailMapper mapper;
	
	public List<DicDetail> selectByItemAndKey(String itemId, Integer type, String key) {
		return mapper.selectByItemAndKey(itemId, type, key);
	}

}
