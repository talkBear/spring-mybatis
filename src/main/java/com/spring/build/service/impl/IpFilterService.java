package com.spring.build.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.build.entity.IpFilter;
import com.spring.build.mapper.IpFilterMapper;
import com.spring.build.service.IIpFilterService;

@Service
public class IpFilterService extends BaseService<IpFilter> implements IIpFilterService  {

	@Autowired
	private IpFilterMapper mapper;
	
	public List<IpFilter> selectByObjectAndPolicy(String objectId, String objectType, Integer policy) {
		return mapper.selectByObjectAndPolicy(objectId, objectType, policy);
	}

}
