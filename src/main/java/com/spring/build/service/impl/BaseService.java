package com.spring.build.service.impl;

import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;

import com.spring.build.service.IBaseService;
import tk.mybatis.mapper.common.Mapper;

public class BaseService<T> implements IBaseService<T> {
	
	@Autowired
	protected Mapper<T> baseMapper;

	public T selectOne(T record) {
		return baseMapper.selectOne(record);
	}

	public List<T> select(T record) {
		return baseMapper.select(record);
	}

	public List<T> selectAll() {
		return baseMapper.selectAll();
	}

	public int selectCount(T record) {
		return baseMapper.selectCount(record);
	}

	public T selectByPrimaryKey(Object key) {
		return baseMapper.selectByPrimaryKey(key);
	}

	public boolean existsWithPrimaryKey(Object key) {
		return baseMapper.existsWithPrimaryKey(key);
	}

	public int insert(T record) {
		return baseMapper.insert(record);
	}

	public int insertSelective(T record) {
		return baseMapper.insertSelective(record);
	}

	public int updateByPrimaryKey(T record) {
		return baseMapper.updateByPrimaryKey(record);
	}

	public int updateByPrimaryKeySelective(T record) {
		return baseMapper.updateByPrimaryKeySelective(record);
	}

	public int delete(T record) {
		return baseMapper.delete(record);
	}

	public int deleteByPrimaryKey(Object key) {
		return baseMapper.deleteByPrimaryKey(key);
	}

	public List<T> selectByExample(Object example) {
		return baseMapper.selectByExample(example);
	}

	public T selectOneByExample(Object example) {
		return baseMapper.selectOneByExample(example);
	}

	public int selectCountByExample(Object example) {
		return baseMapper.selectCountByExample(example);
	}

	public int deleteByExample(Object example) {
		return baseMapper.deleteByExample(example);
	}

	public int updateByExample(T record, Object example) {
		return baseMapper.updateByExample(record, example);
	}

	public int updateByExampleSelective(T record, Object example) {
		return baseMapper.updateByExampleSelective(record, example);
	}

	public List<T> selectByExampleAndRowBounds(Object example, RowBounds rowBounds) {
		return baseMapper.selectByExampleAndRowBounds(example, rowBounds);
	}

	public List<T> selectByRowBounds(T record, RowBounds rowBounds) {
		return baseMapper.selectByRowBounds(record, rowBounds);
	}

}
