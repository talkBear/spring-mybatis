package com.spring.build.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.build.entity.Organization;
import com.spring.build.mapper.OrgMapper;
import com.spring.build.service.IOrgService;

@Service
public class OrgService extends BaseService<Organization> implements IOrgService {

	@Autowired
	private OrgMapper mapper;

	
	public List<Organization> selectByKey(Integer type, String key) {
		return mapper.selectByKey(type, key);
	}

}
