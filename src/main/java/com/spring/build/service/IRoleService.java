package com.spring.build.service;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.spring.build.entity.Role;


public interface IRoleService extends IBaseService<Role> {
	
	/**
	 * 查询角色
	 * <p>根据分类查询</p>
	 * @param category 分类
	 * 		<p>1：角色   2：岗位   3：职位   4：工作组</p>
	 * @return 角色
	 */
	List<Role> selectByCate(@NotNull Integer category);
	
	/**
	 * 查询角色
	 * <p>根据分类和关键字查询</p>
	 * @param category 分类
	 * 		<p>1：角色   2：岗位   3：职位   4：工作组</p>
	 * @param type 关键字类别
	 * 		<p>0：角色名称   1：角色编号</p>
	 * @param key 关键字
	 * @return 角色
	 */
	List<Role> selectByCateAndKey(@NotNull Integer category, Integer type, @NotNull String key);
	
}
