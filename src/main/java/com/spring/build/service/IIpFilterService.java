package com.spring.build.service;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.spring.build.entity.IpFilter;


public interface IIpFilterService extends IBaseService<IpFilter> {

	/**
	 * 查询ip过滤策略
	 * @param objectId 对象Id
	 * @param objectType 对象类型
	 * @param policy 0：拒绝访问，1：允许访问
	 * @return 过滤策略
	 */
	List<IpFilter> selectByObjectAndPolicy(@NotNull String objectId, String objectType, Integer policy);
	
}
