package com.spring.build.service;

import com.spring.build.entity.Model;

public interface IModelService extends IBaseService<Model> {
	
}
