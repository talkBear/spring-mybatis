package com.spring.build.mapper;

import java.util.List;

import com.spring.build.entity.Area;

import tk.mybatis.mapper.common.Mapper;


public interface AreaMapper extends Mapper<Area> {
	
	/**
	 * 下属地区
	 * <p>根据上级地区码，查询下属地区</P>
	 * @param parentId 上级地区码
	 * @return 下属地区
	 */
	List<Area> selectMembers(String parentId);
	
}
