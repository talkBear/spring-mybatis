package com.spring.build.mapper;

import com.spring.build.entity.Model;

import tk.mybatis.mapper.common.Mapper;

public interface ModelMapper extends Mapper<Model> {
	
}
