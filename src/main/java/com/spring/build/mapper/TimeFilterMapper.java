package com.spring.build.mapper;

import com.spring.build.entity.TimeFilter;

import tk.mybatis.mapper.common.Mapper;

public interface TimeFilterMapper extends Mapper<TimeFilter> {

}
