package com.spring.build.mapper;

import java.util.List;

import com.spring.build.entity.AuthResource;

import tk.mybatis.mapper.common.Mapper;


public interface AuthResMapper extends Mapper<AuthResource> {
	
	/**
	 * 查询已授权的功能
	 * @param category 对象分类
	 * 		<p>1-部门2-角色3-岗位4-职位5-工作组</p>
	 * @param objectId 对象id
	 * @return 授权功能
	 */
	List<AuthResource> selectByCateAndObject(Integer category, String objectId);
}
