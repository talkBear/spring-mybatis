package com.spring.build.mapper;

import com.spring.build.entity.Button;

import tk.mybatis.mapper.common.Mapper;

public interface ButtonMapper extends Mapper<Button> {
	
}
