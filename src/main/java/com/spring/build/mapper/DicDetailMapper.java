package com.spring.build.mapper;

import java.util.List;

import com.spring.build.entity.DicDetail;

import tk.mybatis.mapper.common.Mapper;


public interface DicDetailMapper extends Mapper<DicDetail> {

	/**
	 * 分类明细
	 * <p>根据分类和关键字查询分类明细</p>
	 * @param itemId 分类Id
	 * @param type 关键字类型  
	 * 		<p>0：名称	1：值	2：快速查询</p>
	 * 		<p><b> type为空且key也为空，则查询分类下的所有明细</b></p>
	 * 		<p><b>type为空或超出取值范围时，取0</b></p>
	 * @param key 关键字
	 * @return 明细
	 */
	List<DicDetail> selectByItemAndKey(String itemId, Integer type, String key);
	
}
