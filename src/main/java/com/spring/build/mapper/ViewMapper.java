package com.spring.build.mapper;

import com.spring.build.entity.View;

import tk.mybatis.mapper.common.Mapper;

public interface ViewMapper extends Mapper<View>  {

}
