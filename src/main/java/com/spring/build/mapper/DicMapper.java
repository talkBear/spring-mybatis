package com.spring.build.mapper;

import com.spring.build.entity.ProDictionary;

import tk.mybatis.mapper.common.Mapper;

public interface DicMapper extends Mapper<ProDictionary> {

}
