package com.spring.build.mapper;

import javax.validation.constraints.NotNull;

import com.spring.build.entity.RoleZone;

import tk.mybatis.mapper.common.Mapper;

public interface RoleZoneMapper extends Mapper<RoleZone> {
	
	/**
	 * 查询授权数据
	 * @param category 对象分类
	 * 		<p>1：部门   2：角色   3：岗位   4：职位   5：工作组</p>
	 * 		<p>category为空时，查询对象的所有分类下的授权数据</p>
	 * @param objectId 对象主键
	 * @return 授权数据
	 */
	RoleZone selectByCateAndObject(Integer category, @NotNull String objectId);
}
