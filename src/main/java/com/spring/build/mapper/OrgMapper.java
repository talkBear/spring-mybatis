package com.spring.build.mapper;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.spring.build.entity.Organization;

import tk.mybatis.mapper.common.Mapper;


public interface OrgMapper extends Mapper<Organization>{
	
	/**
	 * 查询组织
	 * <p>根据关键字查询组织</p>
	 * @param type 关键字类型 
	 * 		<p>0：机构名称    1：机构外文名称	2：中文名称   3：负责人</p>
	 * @param key 关键字
	 * @return 组织
	 */
	List<Organization> selectByKey(Integer type, @NotNull String key);
	
}
