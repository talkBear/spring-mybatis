package com.spring.build.utils.exceptions;

/**
 * 字段注解异常
 * @author Administrator
 *
 */
@SuppressWarnings("serial")
public class ColLabeledEx extends Exception {
	
	public ColLabeledEx() {
		super();
	}
	
	public ColLabeledEx(String message) {
		super(message);
	}

}
