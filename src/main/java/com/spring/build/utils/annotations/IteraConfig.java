package com.spring.build.utils.annotations;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * 迭代工具的字段注解
 * @author Administrator
 *
 */
@Documented
@Target(ElementType.FIELD)
@Retention(RUNTIME)
public @interface IteraConfig {
	
	public enum ColumnType {
		
		/**
		 * 父节点字段
		 */
		PARENT,
		
		/**
		 * 子节点字段
		 */
		CHILD,
		
		/**
		 * 子List字段
		 */
		LIST

	}
	
	ColumnType value() default ColumnType.PARENT;
	
}
