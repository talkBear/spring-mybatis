package com.spring.build.utils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Id;

import org.apache.commons.beanutils.BeanUtils;

import com.spring.build.utils.annotations.IteraConfig;
import com.spring.build.utils.exceptions.ColLabeledEx;

public class IterationUtil {
	
	/**
	 * 遍历树结构
	 * <p>根节点id为"0"</p>
	 * @param list 从数据库查询出来的无序list
	 * @return 树形结构list
	 * @throws NoSuchFieldException field不存在
	 * @throws SecurityException 获取field时异常
	 * @throws IllegalArgumentException 获取field值时，参数异常
	 * @throws IllegalAccessException 无法获取field的值
	 * @throws InvocationTargetException 执行方法异常
	 * @throws ColLabeledEx 字段注解异常
	 */
	public static <T> List<T> loop(List<T> list) throws NoSuchFieldException, SecurityException, 
			IllegalArgumentException, IllegalAccessException, InvocationTargetException, ColLabeledEx {
		return loop(list, "0");
	}

	/**
	 * 遍历树结构
	 * @param list 从数据库查询出来的无序list
	 * @param rootId 根节点id
	 * @return 树形结构list
	 * @throws NoSuchFieldException field不存在
	 * @throws SecurityException 获取field时异常
	 * @throws IllegalArgumentException 获取field值时，参数异常
	 * @throws IllegalAccessException 无法获取field的值
	 * @throws InvocationTargetException 执行方法异常
	 * @throws ColLabeledEx 字段注解异常
	 */
	public static <T, I> List<T> loop(List<T> list, I rootId) throws NoSuchFieldException, SecurityException, 
			IllegalArgumentException, IllegalAccessException, InvocationTargetException, ColLabeledEx {
		if (list != null && list.size() > 0) {
			T t = list.get(0);
			Map<String, String> iteraKeys = getIteraKeys(t.getClass());
			return loop(list, rootId, iteraKeys.get("parentKey"), iteraKeys.get("childKey"), iteraKeys.get("listKey"));
		}
		return list;
	}
	
	/**
	 * 遍历树结构
	 * @param list 从数据库查询出来的无序list
	 * @param rootId 根节点id
	 * @param parentKey 父节点fieldName
	 * @param childKey 子节点fieldName
	 * @param listKey 子List fieldName
	 * @return 树形结构list
	 * @throws NoSuchFieldException field不存在
	 * @throws SecurityException 获取field时异常
	 * @throws IllegalArgumentException 获取field值时，参数异常
	 * @throws IllegalAccessException 获取field的值的方法，不允许执行
	 * @throws InvocationTargetException 获取field值时，执行方法异常
	 */
	public static <T, I> List<T> loop(List<T> list, I rootId, String parentKey, 
			String childKey, String listKey) throws NoSuchFieldException, SecurityException, 
				IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		Map<I, List<T>> subMap = getSubMap(list, parentKey);
		return loop(subMap, rootId, childKey, listKey);
	}
	
	private static <T, I> List<T> loop(Map<I, List<T>> map, I parentId, String childKey, String listKey) 
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, 
					IllegalAccessException, InvocationTargetException {
		List<T> nodes = map.get(parentId);
		if (nodes != null) {
			for (T node : nodes) {
				I nodeId = getKey(node, childKey);
				List<T> children = loop(map, nodeId, childKey, listKey);
				setNodeChildren(node, listKey, children);
			}
		}
		return nodes;
	}
	
	private static <T> T setNodeChildren(T node, String fieldName, List<T> children) 
			throws IllegalAccessException, InvocationTargetException {
		BeanUtils.setProperty(node, fieldName, children);
		return node;
	}
	
	private static <T, I> Map<I, List<T>> getSubMap(List<T> list, String parentKey) throws NoSuchFieldException, 
			SecurityException, IllegalArgumentException, IllegalAccessException {
		Map<I, List<T>> map = new HashMap<I, List<T>>();
		for (T t : list) {
			I parentId = getKey(t, parentKey);
			List<T> subList = map.get(parentId);
			if (null == subList) {
				subList = new ArrayList<T>();
			}
			subList.add(t);
			map.put(parentId, subList);
		}
		return map;
	}
	
	@SuppressWarnings("unchecked")
	private static <T, I> I getKey(T node, String fieldName) throws NoSuchFieldException, SecurityException, 
						IllegalArgumentException, IllegalAccessException {
		Class<T> clazz = (Class<T>) node.getClass();
		Field field = clazz.getDeclaredField(fieldName);
		field.setAccessible(true);
		return (I) field.get(node);
	}
	
	private static <T> Map<String, String> getIteraKeys(Class<T> clazz) throws ColLabeledEx {
		Map<String, String> iteraKeys = new HashMap<String, String>();
		Field[] fields = clazz.getDeclaredFields();
		List<String> parentKeys = new ArrayList<String>();
		List<String> childKeys = new ArrayList<String>();
		List<String> idKeys = new ArrayList<String>();
		List<String> listKeys = new ArrayList<String>();
		for (Field field : fields) {
			IteraConfig iteraConfig = field.getAnnotation(IteraConfig.class);
			String fieldName = field.getName();
			if (iteraConfig != null) {
				IteraConfig.ColumnType type = iteraConfig.value();
				switch (type) {
				case PARENT:
					parentKeys.add(fieldName);
					break;
				case CHILD:
					childKeys.add(fieldName);
					break;
				case LIST:
					listKeys.add(fieldName);
					break;
				}
			} else if (field.isAnnotationPresent(Id.class)) {
				idKeys.add(fieldName);
			}
		}
		int parentKeysSize = parentKeys.size();
		int childKeysSize = childKeys.size();
		int listKeysSize = listKeys.size();
		int idKeysSize = idKeys.size();
		if (parentKeysSize == 0 || parentKeysSize > 1) {
			throw new ColLabeledEx("Entity can only hold one column labeled by @IteraConfig with value ColumnType.PARENT_KEY");
		}
		iteraKeys.put("parentKey", parentKeys.get(0));
		if (listKeysSize == 0 || listKeysSize > 1) {
			throw new ColLabeledEx("Entity can only hold one column labeled by @IteraConfig with value ColumnType.CHILDREN_LIST");
		}
		iteraKeys.put("listKey", listKeys.get(0));
		if (childKeysSize == 0) {
			if (idKeysSize == 0) {
				throw new ColLabeledEx("Entity must labeled by @Id, or labeled by @IteraConfig with value ColumnType.CHILDREN_KEY");
			} else if (idKeysSize > 1) {
				throw new ColLabeledEx("Entity can only hold one column labeled by @Id when Entity without @IteraConfig");
			} else {
				iteraKeys.put("childKey", idKeys.get(0));
			}
		} else if (childKeysSize > 1) {
			throw new ColLabeledEx("Entity can only hold one column labeled by @IteraConfig with value ColumnType.CHILDREN_KEY");
		} else {
			iteraKeys.put("childKey", childKeys.get(0));
		}
		return iteraKeys;
	}
	
}
