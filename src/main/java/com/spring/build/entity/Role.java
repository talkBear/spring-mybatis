package com.spring.build.entity;

import java.util.Date;

import javax.persistence.Id;
import javax.persistence.Table;


@Table(name = "base_role")
public class Role {
	
	@Id
	private String 	roleId;			//角色主键
	
	private String 	organizeId;		//机构主键
	private Integer category;		//分类1-角色2-岗位3-职位4-工作组
	private String 	enCode;			//角色编码
	private String	fullName;		//角色名称
	private Integer isPublic;		//公共角色
	private Date 	overdueTime;	//过期时间
	private Integer sortCode;		//排序码
	private Integer deleteMark;		//删除标记
	private Integer enabledMark;	//有效标志
	private String 	description;	//备注
	
	private Date 	createDate;		//创建日期
	
	private String 	createUserId;	//创建用户主键
	
	private String 	createUserName;	//创建用户
	
	private Date 	modifyDate;		//修改日期
	
	private String 	modifyUserId;	//修改用户主键
	
	private String 	modifyUserName;	//修改用户
	
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getOrganizeId() {
		return organizeId;
	}
	public void setOrganizeId(String organizeId) {
		this.organizeId = organizeId;
	}
	public Integer getCategory() {
		return category;
	}
	public void setCategory(Integer category) {
		this.category = category;
	}
	public String getEnCode() {
		return enCode;
	}
	public void setEnCode(String enCode) {
		this.enCode = enCode;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public Integer getIsPublic() {
		return isPublic;
	}
	public void setIsPublic(Integer isPublic) {
		this.isPublic = isPublic;
	}
	public Date getOverdueTime() {
		return overdueTime;
	}
	public void setOverdueTime(Date overdueTime) {
		this.overdueTime = overdueTime;
	}
	public Integer getSortCode() {
		return sortCode;
	}
	public void setSortCode(Integer sortCode) {
		this.sortCode = sortCode;
	}
	public Integer getDeleteMark() {
		return deleteMark;
	}
	public void setDeleteMark(Integer deleteMark) {
		this.deleteMark = deleteMark;
	}
	public Integer getEnabledMark() {
		return enabledMark;
	}
	public void setEnabledMark(Integer enabledMark) {
		this.enabledMark = enabledMark;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
	public String getCreateUserName() {
		return createUserName;
	}
	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}
	public Date getModifyDate() {
		return modifyDate;
	}
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
	public String getModifyUserId() {
		return modifyUserId;
	}
	public void setModifyUserId(String modifyUserId) {
		this.modifyUserId = modifyUserId;
	}
	public String getModifyUserName() {
		return modifyUserName;
	}
	public void setModifyUserName(String modifyUserName) {
		this.modifyUserName = modifyUserName;
	}
	
	public static final class Category {
		
		public static final Integer ROLE = 1;
		public static final Integer POST = 2;
		public static final Integer DUTY = 3;
		public static final Integer GROUP = 4;
		
	}
	
}
