package com.spring.build.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.spring.build.utils.annotations.IteraConfig;
import com.spring.build.utils.annotations.IteraConfig.ColumnType;


@Table(name = "base_module")
public class Model {
	
	@Id
	private String 	moduleId;//功能主键
	
	@IteraConfig
	private String 	parentId;//父级主键
	private String 	enCode;//编码
	private String 	fullName;//名称
	private String 	icon;//图标
	private String 	urlAddress;//导航地址
	private String 	target;//导航目标
	private Integer isMenu;//菜单选项
	private Integer allowExpand;//允许展开
	private Integer asPublic;//是否公开
	private Integer allowEdit;//允许编辑
	private Integer allowDelete;//允许删除
	private Integer sortCode;//排序码
	private Integer deleteMark;//删除标记
	private Integer enabledMark;//有效标志
	private String 	description;//备注
	
	private Date 	createDate;//创建日期
	
	private String 	createUserId;//创建用户主键
	
	private String 	createUserName;//创建用户
	
	private Date 	modifyDate;//修改日期
	
	private String 	modifyUserId;//修改用户主键
	
	private String 	modifyUserName;//修改用户
	
	@IteraConfig(ColumnType.LIST)
	@Transient
	private List<Model> children;
	
	public String getModuleId() {
		return moduleId;
	}
	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getEnCode() {
		return enCode;
	}
	public void setEnCode(String enCode) {
		this.enCode = enCode;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getUrlAddress() {
		return urlAddress;
	}
	public void setUrlAddress(String urlAddress) {
		this.urlAddress = urlAddress;
	}
	public String getTarget() {
		return target;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	public Integer getIsMenu() {
		return isMenu;
	}
	public void setIsMenu(Integer isMenu) {
		this.isMenu = isMenu;
	}
	public Integer getAllowExpand() {
		return allowExpand;
	}
	public void setAllowExpand(Integer allowExpand) {
		this.allowExpand = allowExpand;
	}
	public Integer getAsPublic() {
		return asPublic;
	}
	public void setAsPublic(Integer asPublic) {
		this.asPublic = asPublic;
	}
	public Integer getAllowEdit() {
		return allowEdit;
	}
	public void setAllowEdit(Integer allowEdit) {
		this.allowEdit = allowEdit;
	}
	public Integer getAllowDelete() {
		return allowDelete;
	}
	public void setAllowDelete(Integer allowDelete) {
		this.allowDelete = allowDelete;
	}
	public Integer getSortCode() {
		return sortCode;
	}
	public void setSortCode(Integer sortCode) {
		this.sortCode = sortCode;
	}
	public Integer getDeleteMark() {
		return deleteMark;
	}
	public void setDeleteMark(Integer deleteMark) {
		this.deleteMark = deleteMark;
	}
	public Integer getEnabledMark() {
		return enabledMark;
	}
	public void setEnabledMark(Integer enabledMark) {
		this.enabledMark = enabledMark;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
	public String getCreateUserName() {
		return createUserName;
	}
	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}
	public Date getModifyDate() {
		return modifyDate;
	}
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
	public String getModifyUserId() {
		return modifyUserId;
	}
	public void setModifyUserId(String modifyUserId) {
		this.modifyUserId = modifyUserId;
	}
	public String getModifyUserName() {
		return modifyUserName;
	}
	public void setModifyUserName(String modifyUserName) {
		this.modifyUserName = modifyUserName;
	}
	public List<Model> getChildren() {
		return children;
	}
	public void setChildren(List<Model> children) {
		this.children = children;
	}
	
}
