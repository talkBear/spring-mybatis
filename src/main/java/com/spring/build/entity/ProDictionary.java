package com.spring.build.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.spring.build.utils.annotations.IteraConfig;
import com.spring.build.utils.annotations.IteraConfig.ColumnType;


@Table(name = "base_dataitem")
public class ProDictionary {

	@Id
	private String  itemId;        //分类主键
	
	@IteraConfig
	private String  parentId;      //父级主键
	
	private String  itemCode;      //分类编码
	private String  itemName;      //分类名称
	private Integer isTree;        //树型结构
	private Integer isNav;         //导航标记
	private Integer sortCode;      //排序码
	private Integer deleteMark;    //删除标记
	private Integer enabledMark;   //有效标志
	private String  description;   //备注
	
	private Date    createDate;    //创建日期
	
	private String  createUserId;  //创建用户主键
	
	private String  createUserName;//创建用户
	
	private Date 	modifyDate;    //修改日期
	
	private String  modifyUserId;  //修改用户主键
	
	private String  modifyUserName;//修改用户
	
	@IteraConfig(ColumnType.LIST)
	@Transient
	private List<ProDictionary> children;
	
	public ProDictionary() {
		
	}
	
	public ProDictionary(String itemId, String parentId,
			String itemCode, String itemName) {
		this.itemId = itemId;
		this.parentId = parentId;
		this.itemCode = itemCode;
		this.itemName = itemName;
		this.deleteMark = 0;
		this.enabledMark = 1;
	}
	  
	public ProDictionary(String itemId, String parentId,
			String itemCode, String itemName, Integer sortCode) {
		this.itemId = itemId;
		this.parentId = parentId;
		this.itemCode = itemCode;
		this.itemName = itemName;
		this.sortCode = sortCode;
		this.deleteMark = 0;
		this.enabledMark = 1;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public Integer getIsTree() {
		return isTree;
	}

	public void setIsTree(Integer isTree) {
		this.isTree = isTree;
	}

	public Integer getIsNav() {
		return isNav;
	}

	public void setIsNav(Integer isNav) {
		this.isNav = isNav;
	}

	public Integer getSortCode() {
		return sortCode;
	}

	public void setSortCode(Integer sortCode) {
		this.sortCode = sortCode;
	}

	public Integer getDeleteMark() {
		return deleteMark;
	}

	public void setDeleteMark(Integer deleteMark) {
		this.deleteMark = deleteMark;
	}

	public Integer getEnabledMark() {
		return enabledMark;
	}

	public void setEnabledMark(Integer enabledMark) {
		this.enabledMark = enabledMark;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}

	public String getCreateUserName() {
		return createUserName;
	}

	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getModifyUserId() {
		return modifyUserId;
	}

	public void setModifyUserId(String modifyUserId) {
		this.modifyUserId = modifyUserId;
	}

	public String getModifyUserName() {
		return modifyUserName;
	}

	public void setModifyUserName(String modifyUserName) {
		this.modifyUserName = modifyUserName;
	}

	public List<ProDictionary> getChildren() {
		return children;
	}

	public void setChildren(List<ProDictionary> children) {
		this.children = children;
	}
	
}
