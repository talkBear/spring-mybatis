package com.spring.build.entity;

import java.util.Date;

import javax.persistence.Id;
import javax.persistence.Table;


@Table(name = "base_userrelation")
public class Relation {
	
	@Id
	private String  userRelationId;//用户关系主键
	
	private String  userId;			//用户主键
	private Integer category;		//分类:1-部门2-角色3-岗位4-职位5-工作组
	private String  objectId;		//对象主键
	private Integer isDefault;		//默认对象
	private Integer sortCode;		//排序码
	
	private Date 	createDate;	//创建时间
	
	private String  createUserId;	//创建用户主键
	
	private String  createUserName;	//创建用户
	 
	public String getUserRelationId() {
		return userRelationId;
	}
	public void setUserRelationId(String userRelationId) {
		this.userRelationId = userRelationId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Integer getCategory() {
		return category;
	}
	public void setCategory(Integer category) {
		this.category = category;
	}
	public String getObjectId() {
		return objectId;
	}
	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}
	public Integer getIsDefault() {
		return isDefault;
	}
	public void setIsDefault(Integer isDefault) {
		this.isDefault = isDefault;
	}
	public Integer getSortCode() {
		return sortCode;
	}
	public void setSortCode(Integer sortCode) {
		this.sortCode = sortCode;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
	public String getCreateUserName() {
		return createUserName;
	}
	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}
	 
}
