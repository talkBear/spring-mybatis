package com.spring.build.entity;

import java.util.Date;

import javax.persistence.Id;
import javax.persistence.Table;


@Table(name = "base_filtertime")
public class TimeFilter {
	@Id
	private String 	filterTimeId;	//过滤时段主键
	
	private String 	objectType;		//对象类型
	private String 	objectId;		//对象Id
	private Integer visitType;		//访问
	private String 	weekDay1;		//星期一
	private String 	weekDay2;		//星期二
	private String 	weekDay3;		//星期三
	private String 	weekDay4;		//星期四
	private String 	weekDay5;		//星期五
	private String  weekDay6;		//星期六
	private String 	weekDay7;		//星期日
	private Integer sortCode;		//排序码
	private Integer deleteMark;		//删除标记
	private Integer enabledMark;	//有效标志
	private String 	description;	//备注
	
	private Date 	createDate;		//创建日期
	
	private String 	createUserId;	//创建用户主键
	
	private String 	createUserName;	//创建用户
	
	private Date 	modifyDate;		//修改日期
	
	private String 	modifyUserId;	//修改用户主键
	
	private String 	modifyUserName;	//修改用户

	public String getFilterTimeId() {
		return filterTimeId;
	}

	public void setFilterTimeId(String filterTimeId) {
		this.filterTimeId = filterTimeId;
	}

	public String getObjectType() {
		return objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	public String getObjectId() {
		return objectId;
	}

	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}

	public Integer getVisitType() {
		return visitType;
	}

	public void setVisitType(Integer visitType) {
		this.visitType = visitType;
	}

	public String getWeekDay1() {
		return weekDay1;
	}

	public void setWeekDay1(String weekDay1) {
		this.weekDay1 = weekDay1;
	}

	public String getWeekDay2() {
		return weekDay2;
	}

	public void setWeekDay2(String weekDay2) {
		this.weekDay2 = weekDay2;
	}

	public String getWeekDay3() {
		return weekDay3;
	}

	public void setWeekDay3(String weekDay3) {
		this.weekDay3 = weekDay3;
	}

	public String getWeekDay4() {
		return weekDay4;
	}

	public void setWeekDay4(String weekDay4) {
		this.weekDay4 = weekDay4;
	}

	public String getWeekDay5() {
		return weekDay5;
	}

	public void setWeekDay5(String weekDay5) {
		this.weekDay5 = weekDay5;
	}

	public String getWeekDay6() {
		return weekDay6;
	}

	public void setWeekDay6(String weekDay6) {
		this.weekDay6 = weekDay6;
	}

	public String getWeekDay7() {
		return weekDay7;
	}

	public void setWeekDay7(String weekDay7) {
		this.weekDay7 = weekDay7;
	}

	public Integer getSortCode() {
		return sortCode;
	}

	public void setSortCode(Integer sortCode) {
		this.sortCode = sortCode;
	}

	public Integer getDeleteMark() {
		return deleteMark;
	}

	public void setDeleteMark(Integer deleteMark) {
		this.deleteMark = deleteMark;
	}

	public Integer getEnabledMark() {
		return enabledMark;
	}

	public void setEnabledMark(Integer enabledMark) {
		this.enabledMark = enabledMark;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}

	public String getCreateUserName() {
		return createUserName;
	}

	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getModifyUserId() {
		return modifyUserId;
	}

	public void setModifyUserId(String modifyUserId) {
		this.modifyUserId = modifyUserId;
	}

	public String getModifyUserName() {
		return modifyUserName;
	}

	public void setModifyUserName(String modifyUserName) {
		this.modifyUserName = modifyUserName;
	}
	
}
