package com.spring.build.entity;

import java.util.Date;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "base_authorize")
public class AuthResource {
	
	public static final class Category {
		public static final Integer DEPARTMENT = 1;
		public static final Integer ROLE = 2;
		public static final Integer POST = 3;
		public static final Integer DUTY = 4;
		public static final Integer GROPU = 5;
	}
	
	public static final class Level {
		public static final Integer MENU = 1;
		public static final Integer BUTTON = 2;
		public static final Integer VIEW = 3;
		public static final Integer TABLE = 4;
	}
	
	@Id
	private String 	authorizeId;//授权功能主键
	private Integer category;//对象分类:1-部门2-角色3-岗位4-职位5-工作组
	private String 	objectId;//对象主键
	private Integer itemType;//项目类型:1-菜单2-按钮3-视图4表单
	private String 	itemId;//项目主键
	private Integer sortCode;//排序码
	
	private Date 	createDate;//创建时间
	
	private String 	createUserId;//创建用户主键
	
	private String 	createUserName;//创建用户
	
	/*@Transient
	private List<AuthResource> children;*/
	
	public String getAuthorizeId() {
		return authorizeId;
	}
	public void setAuthorizeId(String authorizeId) {
		this.authorizeId = authorizeId;
	}
	public Integer getCategory() {
		return category;
	}
	public void setCategory(Integer category) {
		this.category = category;
	}
	public String getObjectId() {
		return objectId;
	}
	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}
	public Integer getItemType() {
		return itemType;
	}
	public void setItemType(Integer itemType) {
		this.itemType = itemType;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public Integer getSortCode() {
		return sortCode;
	}
	public void setSortCode(Integer sortCode) {
		this.sortCode = sortCode;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
	public String getCreateUserName() {
		return createUserName;
	}
	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}
	/*public List<AuthResource> getChildren() {
		return children;
	}
	public void setChildren(List<AuthResource> children) {
		this.children = children;
	}*/
	
}
