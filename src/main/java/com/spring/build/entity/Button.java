package com.spring.build.entity;

import java.util.List;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.spring.build.utils.annotations.IteraConfig;
import com.spring.build.utils.annotations.IteraConfig.ColumnType;


@Table(name = "base_modulebutton")
public class Button {
	
	@Id
	private String 	moduleButtonId;//按钮主键
	
	private String 	moduleId;//功能主键
	
	@IteraConfig
	private String 	parentId;//父级主键
	private String 	icon;//图标
	private String 	enCode;//编码
	private String 	fullName;//名称
	private String  actionAddress;//Action地址
	private Integer sortCode;//排序码
	
	@IteraConfig(ColumnType.LIST)
	@Transient
	private List<Button> children;

	public String getModuleButtonId() {
		return moduleButtonId;
	}

	public void setModuleButtonId(String moduleButtonId) {
		this.moduleButtonId = moduleButtonId;
	}

	public String getModuleId() {
		return moduleId;
	}

	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getEnCode() {
		return enCode;
	}

	public void setEnCode(String enCode) {
		this.enCode = enCode;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getActionAddress() {
		return actionAddress;
	}

	public void setActionAddress(String actionAddress) {
		this.actionAddress = actionAddress;
	}

	public Integer getSortCode() {
		return sortCode;
	}

	public void setSortCode(Integer sortCode) {
		this.sortCode = sortCode;
	}

	public List<Button> getChildren() {
		return children;
	}

	public void setChildren(List<Button> children) {
		this.children = children;
	}
	
}
