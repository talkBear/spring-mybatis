package com.spring.build.entity;

import java.util.List;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.spring.build.utils.annotations.IteraConfig;
import com.spring.build.utils.annotations.IteraConfig.ColumnType;


@Table(name = "base_modulecolumn")
public class View {

	@Id
	private String 	moduleColumnId;	//列主键
	
	private String 	moduleId;		//功能主键
	
	@IteraConfig
	private String 	parentId;		//父级主键
	private String 	enCode;			//编码
	private String 	fullName;		//名称
	private Integer sortCode;		//排序码
	private String 	description;	//备注
	
	@IteraConfig(ColumnType.LIST)
	@Transient
	private List<View> children;
	
	public String getModuleColumnId() {
		return moduleColumnId;
	}
	public void setModuleColumnId(String moduleColumnId) {
		this.moduleColumnId = moduleColumnId;
	}
	public String getModuleId() {
		return moduleId;
	}
	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getEnCode() {
		return enCode;
	}
	public void setEnCode(String enCode) {
		this.enCode = enCode;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public Integer getSortCode() {
		return sortCode;
	}
	public void setSortCode(Integer sortCode) {
		this.sortCode = sortCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<View> getChildren() {
		return children;
	}
	public void setChildren(List<View> children) {
		this.children = children;
	}
	
}
