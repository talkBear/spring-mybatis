package com.spring.build.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.spring.build.utils.annotations.IteraConfig;
import com.spring.build.utils.annotations.IteraConfig.ColumnType;


@Table(name = "base_organize")
public class Organization {
	
	@Id
	private String 	organizeId;		//机构主键
	
	private Integer category;		//机构分类
	
	@IteraConfig
	private String 	parentId;		//父级主键
	
	private String 	enCode;			//机构代码
	private String 	shortName;		//机构简称
	private String 	fullName;		//机构名称
	private String 	nature;			//机构性质
	private String 	outerPhone;		//外线电话
	private String 	innerPhone;		//内线电话
	private String 	fax;			//传真
	private String 	postalcode;		//邮编
	private String 	email;			//电子邮箱
	private String 	managerId;		//负责人主键
	private String 	manager;		//负责人
	private String 	provinceId;		//省主键
	private String 	cityId;			//市主键
	private String 	countyId;		//县/区主键
	private String 	address;		//详细地址
	private String 	webAddress;		//公司主页
	private Date    foundedTime;	//成立时间
	private String  businessScope;	//经营范围
	private Integer layer;			//层
	private Integer sortCode;		//排序码
	private Integer deleteMark;		//删除标记
	private Integer enabledMark;	//有效标志
	private String  description;	//备注
	
	private Date    createDate;		//创建日期
	
	private String  createUserId;	//创建用户主键
	
	private String  createUserName;	//创建用户
	
	private Date    modifyDate;		//修改日期
	
	private String  modifyUserId;	//修改用户主键
	
	private String  modifyUserName;	//修改用户
	
	@IteraConfig(ColumnType.LIST)
	@Transient
	private List<Organization> children;
	
	public String getOrganizeId() {
		return organizeId;
	}
	public void setOrganizeId(String organizeId) {
		this.organizeId = organizeId;
	}
	public Integer getCategory() {
		return category;
	}
	public void setCategory(Integer category) {
		this.category = category;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getEnCode() {
		return enCode;
	}
	public void setEnCode(String enCode) {
		this.enCode = enCode;
	}
	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getNature() {
		return nature;
	}
	public void setNature(String nature) {
		this.nature = nature;
	}
	public String getOuterPhone() {
		return outerPhone;
	}
	public void setOuterPhone(String outerPhone) {
		this.outerPhone = outerPhone;
	}
	public String getInnerPhone() {
		return innerPhone;
	}
	public void setInnerPhone(String innerPhone) {
		this.innerPhone = innerPhone;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getPostalcode() {
		return postalcode;
	}
	public void setPostalcode(String postalcode) {
		this.postalcode = postalcode;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getManagerId() {
		return managerId;
	}
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	public String getManager() {
		return manager;
	}
	public void setManager(String manager) {
		this.manager = manager;
	}
	public String getProvinceId() {
		return provinceId;
	}
	public void setProvinceId(String provinceId) {
		this.provinceId = provinceId;
	}
	public String getCityId() {
		return cityId;
	}
	public void setCityId(String cityId) {
		this.cityId = cityId;
	}
	public String getCountyId() {
		return countyId;
	}
	public void setCountyId(String countyId) {
		this.countyId = countyId;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getWebAddress() {
		return webAddress;
	}
	public void setWebAddress(String webAddress) {
		this.webAddress = webAddress;
	}
	public Date getFoundedTime() {
		return foundedTime;
	}
	public void setFoundedTime(Date foundedTime) {
		this.foundedTime = foundedTime;
	}
	public String getBusinessScope() {
		return businessScope;
	}
	public void setBusinessScope(String businessScope) {
		this.businessScope = businessScope;
	}
	public Integer getLayer() {
		return layer;
	}
	public void setLayer(Integer layer) {
		this.layer = layer;
	}
	public Integer getSortCode() {
		return sortCode;
	}
	public void setSortCode(Integer sortCode) {
		this.sortCode = sortCode;
	}
	public Integer getDeleteMark() {
		return deleteMark;
	}
	public void setDeleteMark(Integer deleteMark) {
		this.deleteMark = deleteMark;
	}
	public Integer getEnabledMark() {
		return enabledMark;
	}
	public void setEnabledMark(Integer enabledMark) {
		this.enabledMark = enabledMark;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
	public String getCreateUserName() {
		return createUserName;
	}
	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}
	public Date getModifyDate() {
		return modifyDate;
	}
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
	public String getModifyUserId() {
		return modifyUserId;
	}
	public void setModifyUserId(String modifyUserId) {
		this.modifyUserId = modifyUserId;
	}
	public String getModifyUserName() {
		return modifyUserName;
	}
	public void setModifyUserName(String modifyUserName) {
		this.modifyUserName = modifyUserName;
	}
	public List<Organization> getChildren() {
		return children;
	}
	public void setChildren(List<Organization> children) {
		this.children = children;
	}
	
}
