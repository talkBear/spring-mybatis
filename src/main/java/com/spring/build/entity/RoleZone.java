package com.spring.build.entity;

import java.util.Date;

import javax.persistence.Id;
import javax.persistence.Table;


@Table(name = "base_authorizedata")
public class RoleZone {
	
	@Id
	private String authorizeDataId;//授权数据主键
	
	private Integer authorizeType;		//-1:仅限本人; -2:仅限本人及下属; -3:所在部门; -4:所在公司; -5:按明细设置
	private Integer category;			//对象分类:1-部门2-角色3-岗位4-职位5-工作组
	private String 	objectId;			//对象主键
	private String 	itemId;				//项目Id
	private String 	itemName;			//项目Id
	private String  resourceId;			//项目名称
	private Integer isRead;				//只读
	private String  authorizeConstraint;//约束表达式
	private Integer sortCode;			//排序码
	
	private Date 	createDate;			//创建时间
	
	private String  createUserId;		//创建用户主键
	
	private String  createUserName;		//创建用户
	
	public String getAuthorizeDataId() {
		return authorizeDataId;
	}

	public void setAuthorizeDataId(String authorizeDataId) {
		this.authorizeDataId = authorizeDataId;
	}

	public Integer getAuthorizeType() {
		return authorizeType;
	}

	public void setAuthorizeType(Integer authorizeType) {
		this.authorizeType = authorizeType;
	}

	public Integer getCategory() {
		return category;
	}

	public void setCategory(Integer category) {
		this.category = category;
	}

	public String getObjectId() {
		return objectId;
	}

	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	public Integer getIsRead() {
		return isRead;
	}

	public void setIsRead(Integer isRead) {
		this.isRead = isRead;
	}

	public String getAuthorizeConstraint() {
		return authorizeConstraint;
	}

	public void setAuthorizeConstraint(String authorizeConstraint) {
		this.authorizeConstraint = authorizeConstraint;
	}

	public Integer getSortCode() {
		return sortCode;
	}

	public void setSortCode(Integer sortCode) {
		this.sortCode = sortCode;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}

	public String getCreateUserName() {
		return createUserName;
	}

	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}

	public static final class Limit {
		
		/**
		 * 仅限本人
		 */
		public static final Integer ONLY_USER = -1;
		
		/**
		 * 下属
		 */
		public static final Integer UNDERLING = -2;
		
		/**
		 * 部门
		 */
		public static final Integer DEPARTMENT = -3;
		
		/**
		 * 公司
		 */
		public static final Integer ORGANIZATION = -4;
		
		/**
		 * 按明细设置
		 */
		public static final Integer DETAIL = -5;
		
	}
	
	public static final class Category {
		
		public static final Integer DEPARTMENT = 1;
		public static final Integer ROLE = 2;
		public static final Integer POST = 3;
		public static final Integer DUTY = 4;
		public static final Integer GROPU = 5;
	}
}
