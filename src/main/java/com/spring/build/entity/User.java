package com.spring.build.entity;

import java.util.Date;

import javax.persistence.Id;
import javax.persistence.Table;


@Table(name = "base_user")
public class User {
	
	@Id
	private String 	userId;			//用户主键
	
	private String 	enCode;			//用户编码
	private String 	account;		//登录账户
	private String 	password;		//登录密码
	private String 	secretkey;		//密码秘钥
	private String 	realName;		//真实姓名
	private String 	nickName;		//呢称
	private String 	headIcon;		//头像
	private String 	quickQuery;		//快速查询
	private String 	simpleSpelling;	//简拼
	private Integer gender;			//性别
	private Date 	birthday;		//生日
	private String 	mobile;			//手机
	private String 	telephone;		//电话
	private String 	email;			//电子邮件
	private String 	oicq;			//QQ号
	private String 	weChat;			//微信号
	private String 	msn;			//MSN
	private String 	managerId;		//主管主键
	private String 	manager;		//主管
	private String 	organizeId;		//机构主键
	private String 	departmentId;	//部门主键
	private String 	roleId;			//角色主键
	private String 	dutyId;			//岗位主键
	private String 	dutyName;		//岗位名称
	private String 	postId;			//职位主键
	private String 	postName;		//职位名称
	private String 	workGroupId;	//工作组主键
	private Integer securityLevel;	//安全级别
	private Integer userOnLine;		//在线状态
	private Integer openId;			//单点登录标识
	private String 	question;		//密码提示问题
	private String 	answerQuestion;	//密码提示答案
	private Integer checkOnLine;	//允许多用户同时登录
	private Date 	allowStartTime;	//允许登录时间开始
	private Date 	allowEndTime;	//允许登录时间结束
	private Date 	lockStartDate;	//暂停用户开始日期
	private Date 	lockEndDate;	//暂停用户结束日期
	private Date 	firstVisit;		//第一次访问时间
	private Date 	previousVisit;	//上一次访问时间
	private Date 	lastVisit;		//最后访问时间
	private Integer logOnCount;		//登录次数
	private Integer sortCode;		//排序码
	private Integer deleteMark;		//删除标记
	private Integer enabledMark;	//有效标志
	private String 	description;	//备注
	
	private Date 	createDate;		//创建日期
	
	private String 	createUserId;	//创建用户主键
	
	private String 	createUserName;	//创建用户
	
	private Date 	modifyDate;		//修改日期
	private String 	modifyUserId;	//修改用户主键
	private String 	modifyUserName;	//修改用户
	
	@Override
	public String toString() {
		return "User [userId=" + userId + ", enCode=" + enCode + ", account=" + account + ", password=" + password
				+ ", secretkey=" + secretkey + ", realName=" + realName + ", nickName=" + nickName + ", headIcon="
				+ headIcon + ", quickQuery=" + quickQuery + ", simpleSpelling=" + simpleSpelling + ", gender=" + gender
				+ ", birthday=" + birthday + ", mobile=" + mobile + ", telephone=" + telephone + ", email=" + email
				+ ", oicq=" + oicq + ", weChat=" + weChat + ", msn=" + msn + ", managerId=" + managerId + ", manager="
				+ manager + ", organizeId=" + organizeId + ", departmentId=" + departmentId + ", roleId=" + roleId
				+ ", dutyId=" + dutyId + ", dutyName=" + dutyName + ", postId=" + postId + ", postName=" + postName
				+ ", workGroupId=" + workGroupId + ", securityLevel=" + securityLevel + ", userOnLine=" + userOnLine
				+ ", openId=" + openId + ", question=" + question + ", answerQuestion=" + answerQuestion
				+ ", checkOnLine=" + checkOnLine + ", allowStartTime=" + allowStartTime + ", allowEndTime="
				+ allowEndTime + ", lockStartDate=" + lockStartDate + ", lockEndDate=" + lockEndDate + ", firstVisit="
				+ firstVisit + ", previousVisit=" + previousVisit + ", lastVisit=" + lastVisit + ", logOnCount="
				+ logOnCount + ", sortCode=" + sortCode + ", deleteMark=" + deleteMark + ", enabledMark=" + enabledMark
				+ ", description=" + description + ", createDate=" + createDate + ", createUserId=" + createUserId
				+ ", createUserName=" + createUserName + ", modifyDate=" + modifyDate + ", modifyUserId=" + modifyUserId
				+ ", modifyUserName=" + modifyUserName + ", toString()=" + super.toString() + "]";
	}
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getEnCode() {
		return enCode;
	}
	public void setEnCode(String enCode) {
		this.enCode = enCode;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getSecretkey() {
		return secretkey;
	}
	public void setSecretkey(String secretkey) {
		this.secretkey = secretkey;
	}
	public String getRealName() {
		return realName;
	}
	public void setRealName(String realName) {
		this.realName = realName;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public String getHeadIcon() {
		return headIcon;
	}
	public void setHeadIcon(String headIcon) {
		this.headIcon = headIcon;
	}
	public String getQuickQuery() {
		return quickQuery;
	}
	public void setQuickQuery(String quickQuery) {
		this.quickQuery = quickQuery;
	}
	public String getSimpleSpelling() {
		return simpleSpelling;
	}
	public void setSimpleSpelling(String simpleSpelling) {
		this.simpleSpelling = simpleSpelling;
	}
	public Integer getGender() {
		return gender;
	}
	public void setGender(Integer gender) {
		this.gender = gender;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getOicq() {
		return oicq;
	}
	public void setOicq(String oicq) {
		this.oicq = oicq;
	}
	public String getWeChat() {
		return weChat;
	}
	public void setWeChat(String weChat) {
		this.weChat = weChat;
	}
	public String getMsn() {
		return msn;
	}
	public void setMsn(String msn) {
		this.msn = msn;
	}
	public String getManagerId() {
		return managerId;
	}
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	public String getManager() {
		return manager;
	}
	public void setManager(String manager) {
		this.manager = manager;
	}
	public String getOrganizeId() {
		return organizeId;
	}
	public void setOrganizeId(String organizeId) {
		this.organizeId = organizeId;
	}
	public String getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getDutyId() {
		return dutyId;
	}
	public void setDutyId(String dutyId) {
		this.dutyId = dutyId;
	}
	public String getDutyName() {
		return dutyName;
	}
	public void setDutyName(String dutyName) {
		this.dutyName = dutyName;
	}
	public String getPostId() {
		return postId;
	}
	public void setPostId(String postId) {
		this.postId = postId;
	}
	public String getPostName() {
		return postName;
	}
	public void setPostName(String postName) {
		this.postName = postName;
	}
	public String getWorkGroupId() {
		return workGroupId;
	}
	public void setWorkGroupId(String workGroupId) {
		this.workGroupId = workGroupId;
	}
	public Integer getSecurityLevel() {
		return securityLevel;
	}
	public void setSecurityLevel(Integer securityLevel) {
		this.securityLevel = securityLevel;
	}
	public Integer getUserOnLine() {
		return userOnLine;
	}
	public void setUserOnLine(Integer userOnLine) {
		this.userOnLine = userOnLine;
	}
	public Integer getOpenId() {
		return openId;
	}
	public void setOpenId(Integer openId) {
		this.openId = openId;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getAnswerQuestion() {
		return answerQuestion;
	}
	public void setAnswerQuestion(String answerQuestion) {
		this.answerQuestion = answerQuestion;
	}
	public Integer getCheckOnLine() {
		return checkOnLine;
	}
	public void setCheckOnLine(Integer checkOnLine) {
		this.checkOnLine = checkOnLine;
	}
	public Date getAllowStartTime() {
		return allowStartTime;
	}
	public void setAllowStartTime(Date allowStartTime) {
		this.allowStartTime = allowStartTime;
	}
	public Date getAllowEndTime() {
		return allowEndTime;
	}
	public void setAllowEndTime(Date allowEndTime) {
		this.allowEndTime = allowEndTime;
	}
	public Date getLockStartDate() {
		return lockStartDate;
	}
	public void setLockStartDate(Date lockStartDate) {
		this.lockStartDate = lockStartDate;
	}
	public Date getLockEndDate() {
		return lockEndDate;
	}
	public void setLockEndDate(Date lockEndDate) {
		this.lockEndDate = lockEndDate;
	}
	public Date getFirstVisit() {
		return firstVisit;
	}
	public void setFirstVisit(Date firstVisit) {
		this.firstVisit = firstVisit;
	}
	public Date getPreviousVisit() {
		return previousVisit;
	}
	public void setPreviousVisit(Date previousVisit) {
		this.previousVisit = previousVisit;
	}
	public Date getLastVisit() {
		return lastVisit;
	}
	public void setLastVisit(Date lastVisit) {
		this.lastVisit = lastVisit;
	}
	public Integer getLogOnCount() {
		return logOnCount;
	}
	public void setLogOnCount(Integer logOnCount) {
		this.logOnCount = logOnCount;
	}
	public Integer getSortCode() {
		return sortCode;
	}
	public void setSortCode(Integer sortCode) {
		this.sortCode = sortCode;
	}
	public Integer getDeleteMark() {
		return deleteMark;
	}
	public void setDeleteMark(Integer deleteMark) {
		this.deleteMark = deleteMark;
	}
	public Integer getEnabledMark() {
		return enabledMark;
	}
	public void setEnabledMark(Integer enabledMark) {
		this.enabledMark = enabledMark;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
	public String getCreateUserName() {
		return createUserName;
	}
	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}
	public Date getModifyDate() {
		return modifyDate;
	}
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
	public String getModifyUserId() {
		return modifyUserId;
	}
	public void setModifyUserId(String modifyUserId) {
		this.modifyUserId = modifyUserId;
	}
	public String getModifyUserName() {
		return modifyUserName;
	}
	public void setModifyUserName(String modifyUserName) {
		this.modifyUserName = modifyUserName;
	}
	
	public class Condition {//TODO 内部类
		
		private String key;
		private String orgId; //组织
		private String depId; //部门
		private String roleId; //角色
		private String dutyId; //职位
		private String postId; //岗位
		private String groupId; //组
		private String managerId;//主管
		
		public Condition() {
			
		}
		
		public Condition(String key) {
			this.key = key;
		}
		
		public Condition(String key, String orgId) {
			this.key = key;
			this.orgId = orgId;
		}
		
		public Condition(String key, String orgId, String depId) {
			this.key = key;
			this.orgId = orgId;
			this.depId = depId;
		}
		
		public Condition(String key, String orgId, String depId, String roleId) {
			this.key = key;
			this.orgId = orgId;
			this.depId = depId;
			this.roleId = roleId;
		}
		
		public String getKey() {
			return key;
		}
		public void setKey(String key) {
			this.key = key;
		}
		public String getOrgId() {
			return orgId;
		}
		public void setOrgId(String orgId) {
			this.orgId = orgId;
		}
		public String getDepId() {
			return depId;
		}
		public void setDepId(String depId) {
			this.depId = depId;
		}
		public String getRoleId() {
			return roleId;
		}
		public void setRoleId(String roleId) {
			this.roleId = roleId;
		}
		public String getDutyId() {
			return dutyId;
		}
		public void setDutyId(String dutyId) {
			this.dutyId = dutyId;
		}
		public String getPostId() {
			return postId;
		}
		public void setPostId(String postId) {
			this.postId = postId;
		}
		public String getGroupId() {
			return groupId;
		}
		public void setGroupId(String groupId) {
			this.groupId = groupId;
		}
		public String getManagerId() {
			return managerId;
		}
		public void setManagerId(String managerId) {
			this.managerId = managerId;
		}
		
	}
}
