package com.spring.build.entity;

import java.util.Date;

import javax.persistence.Id;
import javax.persistence.Table;


@Table(name = "base_filterip")
public class IpFilter {
	
	public static final class EntityType {
		public static final String USER = "Uesr";
		public static final String ROLE = "Role";
	}
	
	public static final class Policy {
		public static final Integer DENY = 0;
		public static final Integer ALLOW = 1;
	}
	
	@Id
	private String 	filterIPId;		//过滤IP主键
	private String 	objectType;		//对象类型
	private String 	objectId;		//对象Id
	private Integer visitType;		//访问 0:：拒绝访问，1：允许访问
	private Integer type;			//类型
	private String 	ipLimit;		//IP
	private Integer sortCode;		//排序码
	private Integer deleteMark;		//删除标记
	private Integer enabledMark;	//有效标志
	private String 	description;	//备注
	
	private Date 	createDate;		//创建日期
	
	private String 	createUserId;	//创建用户主键
	
	private String 	createUserName;	//创建用户
	
	private Date 	modifyDate;		//修改日期
	
	private String  modifyUserId;	//修改用户主键
	
	private String 	modifyUserName;	//修改用户

	public String getFilterIPId() {
		return filterIPId;
	}

	public void setFilterIPId(String filterIPId) {
		this.filterIPId = filterIPId;
	}

	public String getObjectType() {
		return objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	public String getObjectId() {
		return objectId;
	}

	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}

	public Integer getVisitType() {
		return visitType;
	}

	public void setVisitType(Integer visitType) {
		this.visitType = visitType;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getIpLimit() {
		return ipLimit;
	}

	public void setIpLimit(String ipLimit) {
		this.ipLimit = ipLimit;
	}

	public Integer getSortCode() {
		return sortCode;
	}

	public void setSortCode(Integer sortCode) {
		this.sortCode = sortCode;
	}

	public Integer getDeleteMark() {
		return deleteMark;
	}

	public void setDeleteMark(Integer deleteMark) {
		this.deleteMark = deleteMark;
	}

	public Integer getEnabledMark() {
		return enabledMark;
	}

	public void setEnabledMark(Integer enabledMark) {
		this.enabledMark = enabledMark;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}

	public String getCreateUserName() {
		return createUserName;
	}

	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getModifyUserId() {
		return modifyUserId;
	}

	public void setModifyUserId(String modifyUserId) {
		this.modifyUserId = modifyUserId;
	}

	public String getModifyUserName() {
		return modifyUserName;
	}

	public void setModifyUserName(String modifyUserName) {
		this.modifyUserName = modifyUserName;
	}
	
}
