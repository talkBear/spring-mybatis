package com.spring.build.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.spring.build.utils.annotations.IteraConfig;
import com.spring.build.utils.annotations.IteraConfig.ColumnType;


@Table(name = "base_dataitemdetail")
public class DicDetail {
	
	@Id
	private String  itemDetailId;   //明细主键
	
	private String  itemId;   		//分类主键
	
	@IteraConfig
	private String  parentId;   	//父级主键
	
	private String  itemCode;   	//编码
	private String  itemName;   	//名称
	private String  itemValue;   	//值
	private String  quickQuery;     //快速查询
	private String  simpleSpelling; //简拼
	private Integer isDefault;  	//是否默认
	private Integer sortCode;  		//排序码
	private Integer deleteMark;  	//删除标记
	private Integer enabledMark;    //有效标志
	private String  description;    //备注
	
	private Date    createDate; 	//创建日期
	
	private String  createUserId;   //创建用户主键
	
	private String  createUserName; //创建用户
	
	private Date    modifyDate; 	//修改日期
	
	private String  modifyUserId;   //修改用户主键
	
	private String  modifyUserName; //修改用户
	
	@IteraConfig(ColumnType.LIST)
	@Transient
	private List<DicDetail> children;
	
	public DicDetail() {
		
	}
	
	public DicDetail(String itemDetailId, String itemId,
			String parentId, String itemCode,
			String itemName, String itemValue,
			Integer sortCode) {
		
		this.itemDetailId = itemDetailId;
		this.itemId = itemId;
		this.parentId = parentId;
		this.itemCode = itemCode;
		this.itemName = itemName;
		this.itemValue = itemValue;
		this.sortCode = sortCode;
		this.deleteMark = 0;
		this.enabledMark = 1;
	}

	public String getItemDetailId() {
		return itemDetailId;
	}

	public void setItemDetailId(String itemDetailId) {
		this.itemDetailId = itemDetailId;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemValue() {
		return itemValue;
	}

	public void setItemValue(String itemValue) {
		this.itemValue = itemValue;
	}

	public String getQuickQuery() {
		return quickQuery;
	}

	public void setQuickQuery(String quickQuery) {
		this.quickQuery = quickQuery;
	}

	public String getSimpleSpelling() {
		return simpleSpelling;
	}

	public void setSimpleSpelling(String simpleSpelling) {
		this.simpleSpelling = simpleSpelling;
	}

	public Integer getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(Integer isDefault) {
		this.isDefault = isDefault;
	}

	public Integer getSortCode() {
		return sortCode;
	}

	public void setSortCode(Integer sortCode) {
		this.sortCode = sortCode;
	}

	public Integer getDeleteMark() {
		return deleteMark;
	}

	public void setDeleteMark(Integer deleteMark) {
		this.deleteMark = deleteMark;
	}

	public Integer getEnabledMark() {
		return enabledMark;
	}

	public void setEnabledMark(Integer enabledMark) {
		this.enabledMark = enabledMark;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}

	public String getCreateUserName() {
		return createUserName;
	}

	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getModifyUserId() {
		return modifyUserId;
	}

	public void setModifyUserId(String modifyUserId) {
		this.modifyUserId = modifyUserId;
	}

	public String getModifyUserName() {
		return modifyUserName;
	}

	public void setModifyUserName(String modifyUserName) {
		this.modifyUserName = modifyUserName;
	}

	public List<DicDetail> getChildren() {
		return children;
	}

	public void setChildren(List<DicDetail> children) {
		this.children = children;
	}
	
}
